# How to install Valetudo in your Conga

[Valetudo](https://valetudo.cloud/) is a standalone binary which runs on rooted Vacuums and aims to enable the user to operate the robot vacuum without any Cloud Connection whatsoever.

There is no official support for Congas in Valetudo ecosystem. The following guide is highly experimental and unstable.

There are two ways of using Valetudo in your Conga:

1. Standalone. This will install and run Valetudo directly on your Conga.
2. Local Development Setup.

## Prerequisites

You need to be able to [access your Conga by SSH](./rooting-conga-3x90.md).

## Standalone

This procedure is still experimental
### 1. Install
- [`git`](https://git-scm.com/)
- [`npm`](https://www.npmjs.com/)
### 2. Clone a valid Valetudo repo
```
$ git clone https://github.com/adrigzr/Valetudo
```
### 3. Install dependencies
```
$ cd Valetudo
$ npm install
```
### 4. Create default configuration by running valetudo
```
$ npm run start
CTRL + C
```

On first launch, Valetudo will generate a default config file as `/tmp/valetudo_config.json`. Simply stop Valetudo using `CTRL + C` and edit the newly created file. Change the following lines:
```
{
  ...
  "robot": {
    "implementation": "CecotecCongaRobot",
    "implementationSpecificConfig": {
      "ip": "127.0.0.1"
    }
    ...
}
```
If you would like to integrate Valetudo with Home Assistant, you could edit these lines too:
```
{...
 "mqtt": {
    "enabled": true,
    "server": "192.168.XX.YY",
    "port": "1883",
    "clientId": "valetudo",
    "username": "USER",
    "password": "PASS",
    "usetls": false,
    "ca": "",
    "clientCert": "",
    "clientKey": "",
    "qos": 0,
    "identifier": "robot",
    "topicPrefix": "valetudo",
    "autoconfPrefix": "homeassistant",
    "provideMapData": true,
    "homeassistantMapHack": true
  }
  ...
},
```
### 5. After that, you can build the binary file, as a root user:
```
sudo su -
cd <path-to-Valetudo folder>
npm run build
```

### 6. You should copy now the new valetudo binary file generated and the config file to a rooted conga
```
scp ./build/armv7/valetudo root@<your pc ip>:<path>  #In conga 3090 this path could be /mnt/UDISK/ or similar directory you created i.e. mkdir /mnt/UDISK/valetudo
scp /tmp/valetudo_config.json root@<your pc ip>:<path>  #In conga 3090 this path could be /mnt/UDISK/ or similar directory you created i.e. mkdir /mnt/UDISK/valetudo
```
### 7. Create a script file to export the enviroment variable and run the server at boot
```
ssh root@<your pc ip>
vi /etc/init.d/valetudo
```

add this script:
```
#!/bin/sh /etc/rc.common
# File: /etc/init.d/valetudo
# Usage help: /etc/init.d/valetudo
# Example: /etc/init.d/valetudo start
START=85
STOP=99
USE_PROCD=1
PROG=/mnt/UDISK/valetudo/valetudo
CONFIG=/mnt/UDISK/valetudo/valetudo_config.json
start_service() {
  procd_open_instance
  procd_set_param env VALETUDO_CONFIG_PATH=$CONFIG
  procd_set_param command $PROG

  procd_set_param respawn ${respawn_threshold:-3600} ${respawn_timeout:-10} ${respawn_retry:-5}
  procd_close_instance
}
shutdown() {
  echo shutdown
}
```
### 8. Edit hosts file in conga to point to valetudo server and reboot
```
$> ssh root@<conga ip>
$> echo "<your pc ip> cecotec.das.3irobotix.net cecotec.download.3irobotix.net cecotec.log.3irobotix.net cecotec.ota.3irobotix.net eu.das.3irobotics.net eu.log.3irobotics.net eu.ota.3irobotics.net" >> /etc/hosts
$> /etc/init.d/valetudo enable
$> reboot
```

### 9. Check if webserver up and running
If you have your development server running you can access Valetudo WebServer in http://<ip conga>

### 10. Tips to add the integration into Home Assistant
- In Home Assistant (from now HA) configuration file:
```
mqtt:
  discovery: true
  discovery_prefix: homeassistant
```
- In order to get the map card, you can add [this](https://github.com/TheLastProject/lovelace-valetudo-map-card) addon with HACS.
For more info visit the valetudo official docs at https://valetudo.cloud/pages/integrations/home-assistant-integration.html

## Local Development Setup

### 1. Install prerequisites

1. [`git`](https://git-scm.com/)
2. [`npm`](https://www.npmjs.com/) >= 7
3. [`node`](https://nodejs.org/en/) >= 16

### 2. Clone our repository fork

```
$ git clone https://github.com/adrigzr/valetudo
```

### 3. Install dependencies

```
$ cd valetudo
$ npm install
```

### 4. Create default configuration by running valetudo

```
$ npm run start:dev --workspace=backend
CTRL + C
```

On first launch, Valetudo will generate a default config file as `local/valetudo_config.json`. Simply stop Valetudo using `CTRL + C` and edit the newly created file. Change the following lines:

```
{
  ...
  "embedded": false,
  "robot": {
    "implementation": "CecotecCongaRobot",
    ...
  },
  "webserver": {
    "port": 8080,
    ...
  },
  "logLevel": "debug",
  ...
}
```

Please note that Valetudo will replace the configuration with a default one if it fails to parse it correctly.

### 5. Verify configuration and run

```
$ npm run start:dev --workspace=backend
```

If your configuration is correct, Valetudo should now be working on your development host. You should see an output similar to this:

```
[2021-06-10T18:28:17.394Z] [INFO] Loading configuration file: ../local/valetudo_config.json
[2021-06-10T18:28:17.396Z] [INFO] Set Logfile to /tmp/valetudo.log
[2021-06-10T18:28:17.406Z] [INFO] Starting Valetudo 2021.05.0
[2021-06-10T18:28:17.406Z] [INFO] Commit ID: ref: refs/heads/feature-conga
[2021-06-10T18:28:17.406Z] [INFO] Configuration file: ../local/valetudo_config.json
[2021-06-10T18:28:17.406Z] [INFO] Logfile: /tmp/valetudo.log
[2021-06-10T18:28:17.406Z] [INFO] Robot: Cecotec Conga (CecotecCongaRobot)
[2021-06-10T18:28:17.406Z] [INFO] JS Runtime Version: v14.17.0
[2021-06-10T18:28:17.406Z] [INFO] Arch: x64
[2021-06-10T18:28:17.406Z] [INFO] Max Heap Size: 4144 MiB
[2021-06-10T18:28:17.407Z] [INFO] Node Flags:
[2021-06-10T18:28:17.415Z] [INFO] Webserver running on port 8080
```

### 6. Configure your Conga to access your development server

Access your Conga and change the host file:

```
$ ssh root@<conga ip>
# echo "<your pc ip> cecotec.das.3irobotix.net cecotec.download.3irobotix.net cecotec.log.3irobotix.net cecotec.ota.3irobotix.net eu.das.3irobotics.net eu.log.3irobotics.net eu.ota.3irobotics.net" >> /etc/hosts
# reboot
```

Note: To temporarily revert this while needing to use the Conga App, you can comment out the line in /etc/hosts.

After the Conga robot boots again you should see an output similar to this on your development server:

```
[2021-06-09T18:41:26.818Z] [INFO] Added new robot with id '41075'
```

### 7. Access Valetudo WebServer

If you have your development server running you can access Valetudo WebServer over [http://localhost:8080](http://localhost:8080) or using your computer local network ip address.

## Sources

- [Building and Modifying Valetudo](https://valetudo.cloud/pages/development/building-and-modifying-valetudo.html)

## Troubleshooting

### I can't access my Conga via SSH, it says "No route to host"

If your development PC can't reach your Conga robot even though both of them have proper connectivity please check that...

- ... both the development PC and the Conga robot have been assigned IPs on the same subnet range
- ... the Conga robot's network is not restricted for local access (i.e. not a "guest" network)